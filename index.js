/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function userInputData(){
		let fullName = prompt("What is your name?");
		let age = prompt("What is your age?");
		let location = prompt("What is your address?");

		console.log("Hello, " +fullName);
		console.log("You are " +age + " years old.");
		console.log("You live in " + location);
	}
	userInputData();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteBands(){
		let top1 = "The Beatles";
		let top2 = "Mettalica";
		let top3 = "The Eagles";
		let top4 = "L' arc~en~Ciel";
		let top5 = "Eraserheads";

		console.log("1. " +top1);
		console.log("2. " +top2);
		console.log("3. " +top3);
		console.log("4. " +top4);
		console.log("5. " +top5);
	}
	favoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function top5movies (){
		let movies1 = "1. The Godfather";

		let movies2 = "2. The Godfather, Part II";
		let movies3 = "3. ShawShank Redemption";
		let movies4 = "4. To Kill A Mockingbird";
		let movies5 = "5. Psycho";

		console.log(movies1);
		console.log("Rotten Tomatoes Rating: 97%");
		console.log(movies2);
		console.log("Rotten Tomatoes Rating: 96%");
		console.log(movies3);
		console.log("Rotten Tomatoes Rating: 91%");
		console.log(movies4);
		console.log("Rotten Tomatoes Rating: 93%");
		console.log(movies5);
		console.log("Rotten Tomatoes Rating: 96%");
	}
	top5movies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

function printUsers(){
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	}
	printFriends();
}
printUsers();